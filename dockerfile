FROM golang:1.17-alpine3.15 AS binary
RUN apk --no-cache --update add openssl git

WORKDIR /github.com/swagger.yaml

FROM alpine:3.15
LABEL MAINTAINER="anji>"

COPY --from=binary /go/bin/dockerize /usr/local/bin

ENTRYPOINT ["dockerize"]
CMD ["--help"]
